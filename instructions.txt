install venv '$ sudo pip3 install virtualenv'

create venv $ python3 -m venv ssd_inference (this creates a folder called ssd_inference)

cd into the created folder

activate venv $ source bin/activate

update pip $ pip install --upgrade pip==21.2.4

install opencv $ pip install opencv-python==4.1.0.25

(optional: test opencv $python
import cv2 as cv
cv.__version__

Install TensorFlow Lite for Python: https://www.tensorflow.org/lite/guide/python 
$ echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list
$ curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
$ sudo apt-get update
(in my case I also had to remove some packages, if this is required you will be notified when running the following step)
$ sudo apt-get install python3-tflite-runtime


pi@raspberrypi:~/Desktop/Thesis $ cd ssd_inference/
pi@raspberrypi:~/Desktop/Thesis/ssd_inference $ mkdir images
pi@raspberrypi:~/Desktop/Thesis/ssd_inference $ mkdir images/test
pi@raspberrypi:~/Desktop/Thesis/ssd_inference $ cd images/test
pi@raspberrypi:~/Desktop/Thesis/ssd_inference/images/test $ curl -L "https://app.roboflow.com/ds/STBc3dnF4x?key=Fdsm17PWV2" > roboflow.zip; unzip roboflow.zip; rm roboflow.zip

cd ~/Desktop/Thesis/ssd_inference/
wget https://gitlab.com/FabioMotta1/ssd_mobilenet_ready4tflite_inference/-/raw/main/fine_tuned_model.zip
unzip fine_tuned_model.zip
rm fine_tuned_model.zip

wget https://gitlab.com/FabioMotta1/ssd_mobilenet_ready4tflite_inference/-/raw/main/tflite_label_map.txt
