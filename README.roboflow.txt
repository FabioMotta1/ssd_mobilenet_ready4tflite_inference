
Torx - v26 v7
==============================

This dataset was exported via roboflow.ai on September 20, 2021 at 1:43 PM GMT

It includes 1730 images.
Torx are annotated in Tensorflow TFRecord (raccoon) format.

The following pre-processing was applied to each image:

No image augmentation techniques were applied.


